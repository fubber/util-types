<?php
declare(strict_types=1);

namespace Fubber\Util;

use TypeError;
use function is_numeric, is_object, function_exists, method_exists, is_int, is_float, is_string, is_array, is_bool, gettype;

/**
 * Provides methods to test if a value is numeric or can be treated as numeric when using
 * Fubber/Util/Cast.
 */
class Type {

    /**
     * Cast the value to an object type which for scalar values will implement
     * polymorph types.
     *
     * @param mixed $value
     */
    public static function toObject($value) {
        switch (gettype($value)) {
            case 'boolean' :
            case 'int' :
            case 'double' :
            case 'string' :
                return new class($value) implements Types\IStringable, Types\IIntable, Types\IFloatable, Types\IBoolable {
                    protected $value;
                    public function __construct($value) { $this->value = $value; }
                    public function asBool(): bool { return Type::toBool($value); }
                    public function asInt(): int { return Type::toInt($value); }
                    public function asFloat(): float { return Type::toFloat($value); }
                    public function asString(): string { return Type::toString($value); }
                    public function __toString(): string { return $this->asString(); }
                };
            case 'object' :
                return $value;
        }
        return (object) $value;
    }

    /**
     * Cast value to either 'integer' or 'float' as needed
     *
     * @param mixed $value
     */
    public static function toNumeric($value) {
        if (is_object($value)) {
            if (function_exists($f = get_class($value).'__asFloat') || function_exists($f = get_class($value).'__asInt')) {
                return $f($value);
            } elseif (method_exists($value, 'asFloat')) {
                return $value->asFloat();
            } elseif (method_exists($value, 'asInt')) {
                return $value->asInt();
            }
        }
        if (!is_numeric($value)) {
            return 0;
        }
        // This trick makes the value integer or float as needed
        return $value + 0;
    }

    /**
     * Cast value to type 'integer'.
     *
     * @param mixed $value
     */
    public static function toInt($value): int {
        if (is_object($value)) {
            if (function_exists($f = get_class($value).'__asInt')) {
                return $f($value);
            } elseif (method_exists($value, 'asInt')) {
                return $value->asInt();
            } elseif (function_exists($f = get_class($value).'__asFloat')) {
                return (int) $f($value);
            } elseif (method_exists($value, 'asFloat')) {
                return (int) $value->asFloat();
            }
        }
        return (int) $value;
    }

    /**
     * Cast value to type 'float'.
     *
     * @param mixed $value
     */
    public static function toFloat($value): float {
        if (is_object($value)) {
            if (function_exists($f = get_class($value).'__asFloat')) {
                return $f($value);
            } elseif (method_exists($value, 'asFloat')) {
                return $value->asFloat();
            } elseif (function_exists($f = get_class($value).'__asFloat') || function_exists($f = get_class($value).'__asInt')) {
                return $f($value);
            } elseif (method_exists($value, 'asInt')) {
                return (float) $value->asInt();
            }
        }
        return (float) $value;
    }

    /**
     * Cast the value to type 'string'
     *
     * @param mixed $value
     */
    public static function toString($value): string {
        if (is_object($value)) {
            if (function_exists($f = get_class($value).'__asString')) {
                return $f($value);
            } elseif (method_exists($value, 'asString')) {
                return $value->asString();
            } elseif (method_exists($value, '__toString')) {
                return $value->__toString();
            }
        }
        return (string) $value;
    }

    /**
     * Cast the value to type 'boolean'
     *
     * @param mixed $value
     */
    public static function toBool($value): bool {
        if (is_object($value)) {
            if (function_exists($f = get_class($value).'__asBool')) {
                return $f($value);
            } elseif (method_exists($value, 'asBool')) {
                return $value->asBool();
            } elseif (function_exists($f = get_class($value).'__asInt')) {
                return $f($value) !== 0;
            } elseif (method_exists($value, 'asInt')) {
                return $value->asInt() !== 0;
            } elseif (function_exists($f = get_class($value).'__asFloat')) {
                return $f($value) !== 0.0;
            } elseif (method_exists($value, 'asFloat')) {
                return $value->asFloat() !== 0.0;
            } elseif (function_exists($f = get_class($value).'__asString')) {
                return $f($value) !== '';
            } elseif (method_exists($value, 'asString')) {
                return $value->asString() !== '';
            } elseif (method_exists($value, '__toString')) {
                return $value->__toString() !== '';
            }
        }
        return (bool) $value;
    }

    /**
     * Cast the value to type 'array' or ensure it implements ArrayAccess
     *
     * @param mixed $value
     */
    public static function toArray($value) {
        if (is_object($value)) {
            if (function_exists($f = get_class($value).'__asArray')) {
                return $f($value);
            } elseif ($value instanceof \ArrayAccess) {
                return $value;
            }
        }
        return (array) $value;
    }

    /**
     * Type check that value is numeric
     *
     * @param mixed $value
     */
    public static function assertNumeric($value): void {
        if (!static::isNumeric($value)) {
            throw new TypeError('Argument must be a numeric type or implement either \'asInt()\' or \'asFloat()\'');
        }
    }

    /**
     * Type check that value is integer
     *
     * @param mixed $value
     */
    public static function assertInt($value): void {
        if (!static::isInt($value)) {
            throw new TypeError('Argument must be of type int or implement the method \'asInt()\'');
        }
    }

    /**
     * Type check that value is float
     *
     * @param mixed $value
     */
    public static function assertFloat($value): void {
        if (!static::isFloat($value)) {
            throw new TypeError('Argument must be of type float or implement the method \'asFloat()\'');
        }
    }

    /**
     * Type check that value is string
     *
     * @param mixed $value
     */
    public static function assertString($value): void {
        if (!static::isString($value)) {
            throw new TypeError('Argument must be of type string or implement the method \'asString()\'');
        }
    }

    /**
     * Type check that value is array
     *
     * @param mixed $value
     */
    public static function assertArray($value): void {
        if (!static::isArray($value)) {
            throw new TypeError('Argument must be of type array or implement ArrayAccess\'');
        }
    }

    /**
     * Type check that value is boolean
     *
     * @param mixed $value
     */
    public static function assertBool($value): void {
        if (!static::isBool($value)) {
            throw new TypeError('Argument must be of type bool or implement the method \'asBoolean()\'');
        }
    }

    /**
     * Is the value a numeric value?
     *
     * @param mixed $value
     */
    public static function isNumeric($value): bool {
        return is_numeric($value) || (is_object($value) && (method_exists($value, 'asInt') || method_exists($value, 'asFloat') || function_exists(get_class($value).'__asInt') || function_exists(get_class($value).'__asFloat')));
    }

    /**
     * Is the value an integer?
     *
     * @param mixed $value
     */
    public static function isInt($value): bool {
        return is_int($value) || (is_object($value) && (method_exists($value, 'asInt') || function_exists(get_class($value).'__asInt')));
    }

    /**
     * Is the value a float
     *
     * @param mixed $value
     */
    public static function isFloat($value): bool {
        return is_float($value) || (is_object($value) && (method_exists($value, 'asFloat') || function_exists(get_class($value).'__asFloat')));
    }

    /**
     * Is the value a string?
     *
     * @param mixed $value
     */
    public static function isString($value): bool {
        return is_string($value) || (is_object($value) && (method_exists($value, 'asString') || method_exists($value, '__toString') || function_exists(get_class($value).'__asString')));
    }

    /**
     * Is the value an array?
     *
     * @param mixed $value
     */
    public static function isArray($value): bool {
        return is_array($value) || (is_object($value) && ($value instanceof \ArrayAccess || function_exists(get_class($value).'__asArray')));
    }

    /**
     * Is the value a boolean?
     *
     * @param mixed $value
     */
    public static function isBool($value): bool {
        return is_bool($value) || (is_object($value) && (method_exists($value, 'asBool') || function_exists(get_class($value).'__asBool')));
    }
}
