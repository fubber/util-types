<?php
namespace Fubber\Util\Types;

interface IFloatable {
    /**
     * This object represents a float value that can be retrieved by this method
     */
    public function asFloat(): float;
}
