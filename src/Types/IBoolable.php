<?php
namespace Fubber\Util\Types;

interface IBoolable {
    /**
     * This object represents a boolean value which can be retrieved using this method
     */
    public function asBool(): bool;
}
