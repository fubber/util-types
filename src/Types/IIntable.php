<?php
namespace Fubber\Util\Types;

interface IIntable {
    /**
     * This object represents an integer value that can be retrieved by this method
     */
    public function asInt(): int;
}
