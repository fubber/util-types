<?php
namespace Fubber\Util\Types;

interface IStringable /* extends \Stringable */{
    /**
     * This object can be represented by a string retrievable by this method
     */
    public function asString(): string;
}
