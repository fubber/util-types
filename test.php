<?php
declare(strict_types=1);

require(__DIR__.'/vendor/autoload.php');

use Fubber\Util\Type;

foreach ([ 0, 1, 500, 1000.1, true, false, 'hello', '123', null ] as $value) {

    foreach ( [ ['isNumeric', 'toNumeric'], ['isInt', 'toInt'], ['isFloat', 'toFloat'], ['isBool', 'toBool'], ['isString', 'toString'], ['isArray', 'toArray'] ] as $methods ) {

        list($is, $to) = $methods;


        echo "With value ".json_encode($value)."    \tusing Type::$is\t".json_encode(Type::$is($value))."\tusing Type::$to\t".json_encode(Type::$to($value))."\n";

    }

}
